<?php

namespace App\Acme\SumoMail;

use Illuminate\Support\ServiceProvider;

class SumoMailServiceProvider extends ServiceProvider 
{
	public function register()
	{
		$this->app->bind('sumomail', 'App\Acme\SumoMail\SumoMail');
	}
}